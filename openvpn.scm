(define-module (openvpn)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages glib)
  #:use-module (guix utils)
  )


(define-public openvpn3
  (let ((commit "92c63ad9511dfe730416d4ac63c7cd0353638471")
        (revision "1"))
    (package
     (name "openvpn3")
     (version "v24")
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/OpenVPN/openvpn3-linux")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1qhk35i2pscapzyynck39irjwg8ah4a1a9ak781m0g14r1jqhsrx"))))
     (build-system meson-build-system)
     (native-inputs (list iproute pkg-config))
     (inputs (append (list lz4 lzo openssl linux-pam tinyxml2 jsoncpp dbus)
                     (if (target-linux?) (list libcap-ng) '())))
     (home-page "https://openvpn.net/")
     (synopsis "Virtual private network daemon")
     (description
      "OpenVPN implements virtual private network (@dfn{VPN}) techniques
for creating secure point-to-point or site-to-site connections in routed or
bridged configurations and remote access facilities.  It uses a custom
security protocol that utilizes SSL/TLS for key exchange.  It is capable of
traversing network address translators (@dfn{NAT}s) and firewalls.")
     (license license:gpl2))))
