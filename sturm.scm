(define-module (sturm)
  #:use-module (guix build-system go)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages check)
  #:use-module (gnu packages code)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages less)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages man)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages rsync)
  #:use-module (gnu packages shellutils)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages xdisorg))

(define-public sturm-dev
  (package
    (name "sturm-dev")
    (version "0.0.1")
    (source #f)
    (build-system trivial-build-system)
    (arguments
     (list #:builder #~(mkdir #$output)))
    (propagated-inputs
     (list bash
           cloc
           coreutils  ; ls etc.
           diffutils  ; diff
           direnv
           findutils  ; find
           gawk  ; awk
           gcc-toolchain
           git
           (list git "gui")
           gnupg
           grep
           guix
           less
           man-db
           ncurses  ; reset
           openssh
           rsync
           sed
           the-silver-searcher
           util-linux+udev  ; more (PostgreSQL complains without it)
           which
           unzip  ; needed by cloc
           ))
    (synopsis "Sturm development metapackage")
    (description "This metapackage includes a common set of development tools required in a `guix
shell --pure` environment, such as `git`, `find`, `bash`, `gpg` and `ssh`.")
    (license (list))
    (home-page "")))

(define-public sturm-python-dev
  (package
    (name "sturm-python-dev")
    (version "0.0.1")
    (source #f)
    (build-system trivial-build-system)
    (arguments
     (list #:builder #~(mkdir #$output)))
    (propagated-inputs
     (list
      python-toolchain
      ;; So we don't get missing timezone "Australia/Melbourne" on Guix.
      tzdata
      ;; Includes python-wheel so `pip install` can build wheels instead of using
      ;; legacy `setup.py install`.
      python-wheel
      python-tomli))
    (synopsis "Sturm development metapackage")
    (description "This metapackage includes a common set of development tools required in a `guix
shell --pure` environment, such as `git`, `find`, `bash`, `gpg` and `ssh`.")
    (license (list))
    (home-page "")))

(define-public sturm-python-3.12-dev
  (package
    (name "sturm-python-next-dev")
    (version "0.0.1")
    (source #f)
    (build-system trivial-build-system)
    (arguments
     (list #:builder #~(mkdir #$output)))
    (propagated-inputs
     (list python-3.12
           tzdata
           python-wheel
           python-tomli))
    (synopsis "Sturm development metapackage")
    (description "This metapackage includes a common set of development tools required in a `guix
shell --pure` environment, such as `git`, `find`, `bash`, `gpg` and `ssh`.")
    (license (list))
    (home-page "")))
